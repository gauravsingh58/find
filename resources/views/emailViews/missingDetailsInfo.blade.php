<!DOCTYPE html>
<html>
<head>
 <title>Todays Cases</title>
</head>
<body>


<h3>Date: {{@$date}}</h3>

<h3>Sales Manager Name: {{@$sales_mgr_name}}</h3>


<table border="1">
	<thead>
		<tr>
			<th>Type</th>
			<th>No. of Cases</th>
			<th>Gross Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>CASH</td>
			<td>{{@$cash_cases_count}}</td>
			<td>{{@$cash_case_tests_gross}}</td>
		</tr>	
		<tr>
			<td>CREDIT CLIENT</td>
			<td>{{@$credit_client_cases_count}}</td>
			<td>{{@$credit_client_case_tests_gross}}</td>
		</tr>	
		<tr>
			<td>PHARMA</td>
			<td>{{@$pharma_cases_count}}</td>
			<td>{{@$pharma_case_tests_gross}}</td>
		</tr>	
	</tbody>
</table>
<p></p><p></p><p></p>

<!--Cash Cases-->
<h4>CASH CASES:</h4>

@if(count($cash_case_tests)>0)
	<table border="1">
		<thead>
			<tr>
				<th>Case No.</th>
				<th>Patient Name</th>
				<th>Institution Name</th>
				<th>Doctor Name</th>
				<th>Test Name</th>	
				<th>Coupon</th>	
				<th>MRP</th>
				<th>Net Amount</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($cash_case_tests as $cash_case_test)
				<tr>
					<td>{{@$cash_case_test->case_no}}</td>
					<td>{{@$cash_case_test->patient_name}}</td>
					<td>{{@$cash_case_test->institution_name}}</td>
					<td>{{@$cash_case_test->doctor_name}}</td>
					<td>{{@$cash_case_test->test_name}}</td>
					
					<td>{{@$cash_case_test->coupon_name}}</td>

					<td>{{@$cash_case_test->mrp}}</td>
					<td>{{@$cash_case_test->net_amount}}</td>
					<td>{{@$cash_case_test->status}}</td>
				</tr>
			@endforeach
		</tbody>

	</table>
@else
	<p>NO CASH CASES</p>
@endif
<!--Cash Cases End-->

<p>	</p>

<!--Credit Client Cases-->
<h4>CREDIT CLIENT CASES:</h4>
@if(count($credit_client_case_tests)>0)
	<table border="1">
		<thead>
			<tr>
				<th>Case No.</th>
				<th>Patient Name</th>
				<th>Client Name</th>
				<th>Doctor Name</th>
				<th>Test Name</th>	
				
				<th>Coupon</th>	

				<th>MRP</th>
				<th>Net Amount</th>
				<th>Status</th>

			</tr>	
		</thead>
		<tbody>

			@foreach($credit_client_case_tests as $credit_client_case_test)
				<tr>
					<td>{{@$credit_client_case_test->case_no}}</td>
					<td>{{@$credit_client_case_test->patient_name}}</td>
					<td>{{@$credit_client_case_test->client_name}}</td>
					<td>{{@$credit_client_case_test->doctor_name}}</td>
					<td>{{@$credit_client_case_test->test_name}}</td>

					<td>{{@$credit_client_case_test->coupon_name}}</td>

					<td>{{@$credit_client_case_test->mrp}}</td>
					<td>{{@$credit_client_case_test->net_amount}}</td>
					<td>{{@$credit_client_case_test->status}}</td>
				</tr>
			@endforeach
		</tbody>	

	</table>
@else
	<p>NO CREDIT CLIENT CASES</p>
@endif
<!--Credit Client Cases End-->

<p>	</p>

<!--Pharma Cases-->
<h4>PHARMA CASES:</h4>
@if(count($pharma_case_tests)>0)
	<table border="1">
		<thead>
			<tr>
				<th>Case No.</th>
				<th>Patient Name</th>
				<th>Client Name</th>
				<th>Doctor Name</th>
				<th>Test Name</th>	
				
				<th>Coupon</th>	

				<th>MRP</th>
				<th>Net Amount</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($pharma_case_tests as $pharma_case_test)
				<tr>
					<td>{{@$pharma_case_test->case_no}}</td>
					<td>{{@$pharma_case_test->patient_name}}</td>
					<td>{{@$pharma_case_test->client_name}}</td>
					<td>{{@$pharma_case_test->doctor_name}}</td>
					<td>{{@$pharma_case_test->test_name}}</td>

					<td>{{@$pharma_case_test->coupon_name}}</td>

					<td>{{@$pharma_case_test->mrp}}</td>
					<td>{{@$pharma_case_test->net_amount}}</td>
					<td>{{@$pharma_case_test->status}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<p>NO PHARMA CASES</p>
@endif
<!--Pharma Cases End-->
<p></p><p></p><p></p>

<h4>
If you have any questions about the accessioning or anything looks incorrect - please write to coresponse-email@corediagnostics.in .	
</h4>


</body>
</html>
