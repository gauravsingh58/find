<!DOCTYPE html>
<html>
<head>
 <title>Incomplete Cases</title>
</head>
<body>



<h4>List of cases with missing details</h4>

@if(count($err_cases)>0)
	<table border="1">
		<thead>
			<tr>
				<th>Case No.</th>
				<th>Case test id</th>
				<th>Client id</th>
				<th>Project id</th>
				<th>Project code</th>
				<th>Accessioning date</th>
				<th>Collection date</th>
				<th>Institution email</th>	
				
				<th>Phlebotomist email</th>
				<th>Common email</th>

				
			</tr>
		</thead>
		<tbody>
			@foreach($err_cases as $err_case)
				<tr>
					<td>{{@$err_case->case_no}}</td>
					<td>{{@$err_case->case_test_id}}</td>
					<td>{{@$err_case->client_id}}</td>
					<td>{{@$err_case->project_id}}</td>
					@if($err_case->project_id==262){
						<td>ART</td>
					}
					@if($err_case->project_id==263){
						<td>OST</td>
					}
					@if($err_case->client_id==953){
						<td>FIND</td>
					}
					@endif

					<td>{{@$err_case->//}}</td>
					<td>{{@$err_case->accessioning_date}}</td>
					<td>{{@$err_case->collection_date}}</td>
					<td>{{@$err_case->institution_email_id}}</td>
					<td>{{@$err_case->phlebo_email_id}}</td>
					<td>{{@$err_case->common_email_id}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<p>NO CASES</p>
@endif

<p></p><p></p><p></p>

<h4>
If you have any questions about the accessioning or anything looks incorrect - please write to coresponse-email@corediagnostics.in .	
</h4>


</body>
</html>
