<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



// art and ost changes
Route::get("syncProjectCases","projectDataController@syncProjectCases");
Route::get("notifyIncompCases","EmailController@notifyIncompCases");



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get("test","TestingController@test");

Route::get("syncCI","BaseDataController@syncFindClientInstitutions");

Route::get("syncFindCases","BaseDataController@syncFindCases");
//Route::get("test","TestingController@test");

Route::get("dnld-excel","ExcelController@downloadExcel");



Route::get("sendEmail","EmailController@sendEmailReport");
Route::get("queueEmail","EmailController@setEmailQueue");



Route::get("sentCases","CasesController@sentCases");



