<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('case_id')->unsigned();
            $table->integer('client_id');

            $table->integer('uid_serial_number')->nullable()->default(null);
            $table->integer('id1')->nullable()->default(null);
            $table->integer('pid')->nullable()->default(null);
            $table->integer('case_no');
            $table->integer('test_trf_id');
            $table->string('test_code');
            $table->string('test_name');
            $table->string('specimen_detail')->nullable()->default(null);
            $table->string('first_name')->nullable()->default(null);
            $table->string('middle_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            
            $table->string('sex')->nullable()->default(null);
            $table->string('dob')->nullable()->default(null);
            $table->string('age')->nullable()->default(null);
            $table->integer('mob_no')->nullable()->default(null);
            $table->string('labnum')->nullable()->default(null);

            $table->date('sample_collection_date')->nullable()->default(null);
            $table->date('sample_received_date')->nullable()->default(null);
            $table->date('report_date')->nullable()->default(null);

            $table->string('report_document')->nullable()->default(null);
            $table->string('treating_hospital')->nullable()->default(null);
            $table->string('units')->nullable()->default(null);
            $table->string('result')->nullable()->default(null);
            $table->string('result_type')->nullable()->default(null);
            
            $table->tinyInteger('email_sent')->nullable()->default(0);

            $table->date('emailed_at')->nullable()->default(null);
            //$table->integer('c');
            $table->string('accessioning_date')->nullable()->default(null);
            $table->string('reporting_date')->nullable()->default(null);
            $table->integer('workflow_state_id');
            $table->string('current_status');
            //$table->integer('svr');
            $table->string('age_year')->nullable()->default(null);
            $table->string('age_month')->nullable()->default(null);
            $table->string('age_day')->nullable()->default(null);











            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
