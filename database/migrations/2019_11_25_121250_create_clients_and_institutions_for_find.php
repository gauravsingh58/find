<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsAndInstitutionsForFind extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients_f', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('client_id');
            $table->string('client_name');
            $table->string('short_code');

            $table->string('old_short_code')->nullable()->default(null);
            $table->timestamps();
        });


        Schema::create('institutions_f', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('institution_id');
            $table->string('institution_name');
            $table->string('short_code');

            $table->string('old_short_code')->nullable()->default(null);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
