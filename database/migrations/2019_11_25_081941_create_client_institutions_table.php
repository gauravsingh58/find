<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_institution_f', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('institution_id');
            $table->string('institution_email_id')->nullable()->default(null);
            $table->string('phlebo_email_id')->nullable()->default(null);
            $table->string('common_email_id')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_institutions');
    }
}
