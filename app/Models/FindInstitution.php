<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FindInstitution extends Model
{
    protected $table = 'institutions_f';
}
