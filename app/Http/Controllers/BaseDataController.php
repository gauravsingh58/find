<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\FindCase;
use App\Models\FindClient;
use App\Models\FindInstitution;
use Ixudra\Curl\Facades\Curl;
use Mail;

use Log;



use App\Models\FindClientInstitution;

// use App\Models\FindInstitution;

class BaseDataController extends Controller
{
    
    public function syncFindClientInstitutions(){

        $this->syncFindClient();
        $this->syncFindInstitutions();

        dd("done");
    }    

    public function syncFindClient(){

        $data_s=@Curl::to('https://lims.crick.corediagnostics.in/api/getFindClient')
                            ->get(); 

        $data=json_decode($data_s);                    

        
        $check=@FindClient::where('client_id','=',$data->client_id)->first();

        if($check==null){
            $client_entry=new FindClient;
            
        }else{
            $client_entry=$check;
        }

            $client_entry->client_id=$data->client_id;
            $client_entry->client_name=$data->client_name;
            $client_entry->short_code=$data->short_code;
            $client_entry->old_short_code=$data->old_short_code;
            $client_entry->save();

        

    }


    public function syncFindInstitutions(){

        $data_s=@Curl::to('https://lims.crick.corediagnostics.in/api/getFindInstitutions')
                            ->get(); 

        $data=json_decode($data_s);                    

        //dd($data);


        foreach($data as $d){

            $check=@FindClientInstitution::where('client_id','=',$d->client_id)
                                  ->where('institution_id','=',$d->institution_id)
                                  ->first();  

            if($check==null){

                $client_inst=new FindClientInstitution;
                
            }else{
                
                $client_inst=$check;
                
            }   

                $client_inst->client_id=$d->client_id;
                $client_inst->institution_id=$d->institution_id;
                $client_inst->institution_email_id=$d->email;
                $client_inst->save();    


            $check2=FindInstitution::where('institution_id','=',$d->institution_id)
                                        ->first();
            if($check2==null){
                $inst=new FindInstitution;
            }else{
                $inst=$check2;
            }     

                $inst->institution_id=$d->institution_id;
                $inst->institution_name=$d->institution_name;
                $inst->short_code=$d->short_code;
                $inst->old_short_code=$d->old_short_code;
                $inst->save();                                                          

        }

    }


    public function syncFindCases(){

        $data_s=@Curl::to('https://lims.crick.corediagnostics.in/api/getFindCases/update')
                            ->get(); 

        $data=json_decode($data_s);  

        Log::info(" API TO SYNC FIND CASES HAS BEEN HIT ");
        //dd($data);


        foreach($data as $d){

            $data_check=FindCase::where('test_trf_id','=',$d->test_trf_id)->first();

            if($data_check==null){
                $case_entry=new FindCase;
                
            }else{
                $case_entry=$data_check;
            }

                $case_entry->case_id=$d->case_no;
                $case_entry->case_test_id=$d->test_trf_id;
                $case_entry->project_id=$d->project_id;



                $case_entry->coupon_name=$d->svr;
                $case_entry->client_id=$d->client_id;
                $case_entry->institution_id=$d->institution_id;
                $case_entry->uid_serial_number=$d->uid_serial_number;
                $case_entry->id1=$d->id1;
                $case_entry->pid=$d->pid;
                $case_entry->case_no=$d->case_no;
                $case_entry->test_trf_id=$d->test_trf_id;
                $case_entry->test_code=$d->test_code;
                $case_entry->test_name=$d->test_name;
                $case_entry->specimen_detail=$d->specimen_detail;
                $case_entry->first_name=$d->first_name;
                $case_entry->middle_name=$d->middle_name;
                $case_entry->last_name=$d->last_name;
                $case_entry->sex=$d->sex;
                $case_entry->dob=$d->dob;
                $case_entry->age=$d->age;
                $case_entry->mob_no=$d->mob_no;
                $case_entry->labnum=$d->labnum;
                $case_entry->sample_collection_date=$d->sample_collection_date;
                $case_entry->sample_received_date=$d->sample_received_date;
                $case_entry->report_date=$d->report_date;
                $case_entry->report_document=$d->report_document;
                $case_entry->treating_hospital=$d->treating_hospital;
                $case_entry->units=$d->units;
                $case_entry->result=$d->result;
                $case_entry->result_type=$d->result_type;
                //$case_entry->email_sent=0;
                $case_entry->emailed_at=null;
                $case_entry->accessioning_date=null;
                $case_entry->reporting_date=$d->report_date;
                $case_entry->workflow_state_id=$d->workflow_state_id;
                $case_entry->current_status=$d->current_status;
                $case_entry->age_year=$d->ageYear;
                $case_entry->age_month=$d->ageMonth;
                $case_entry->age_day=$d->ageDay;
                $case_entry->save();


        }

        dd("doen");
    }






}




