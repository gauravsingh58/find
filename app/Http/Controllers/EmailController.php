<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FindCase;
use Mail;

use App\Models\EmailQueue;
use App\Models\missingDetails;

use Log;
use DB;


class EmailController extends Controller
{

    public function setEmailQueue(){
        //Log::info("set email q")
        Log::info("API TO QUEUE FIND CASES HAS BEEN HIT");

        ini_set('max_execution_time', 3600);

        $entries=DB::table("cases as c")
                            ->where('c.email_sent','=',0)
                            //->where('c.case_no','=',20023874)
                            
                            /*
                            ->leftJoin('client_institution_f as cif', function ($join) {
                                    $join->on('cif.institution_id', 
                                                            '=', 'c.institution_id');
                            })
                            ->select('c.id','cif.institution_email_id','cif.phlebo_email_id','cif.common_email_id',
                                      'c.client_id','c.institution_id','c.report_document'  )
                            */

                            ->limit(50)

                            ->get();        
        //dd($entries);                    

        foreach($entries as $entry){
            $email_details=$this->getCaseTestEmailDetails($entry->case_test_id);


            $recipients=[   $email_details["inst_email"],
                            $email_details["phlebo_email"],
                            $email_details["comm_email"]
                        ];

            

            foreach($recipients as $recipient){

                if($recipient==null){
                    continue;
                }

                $check=EmailQueue::where('case_report_id','=',$entry->id)
                                    ->where('recipient','=',$recipient)
                                    ->where('report_document','=',$entry->report_document)
                                    ->where('email_sent','=',0)   
                                    ->first();    

                if($check==null){
                    $email_queue=new EmailQueue;


                    $email_queue->case_report_id=$entry->id;
                    $email_queue->client_id=$entry->client_id;
                    $email_queue->institution_id=$entry->institution_id;
                    $email_queue->report_document=$entry->report_document;
                    $email_queue->recipient=$recipient;
                    $email_queue->email_sent=0;
                    $email_queue->emailed_at=null;
                    $email_queue->save();   

                    Log::info(" SETTING UP EMAIL_QUEUE WITH EMAIL_QUEUE_ID ".$email_queue->id);
                }                    


                                              

            }

            
        }                    

        dd("done");

    }


    public function getCaseTestEmailDetails($case_test_id){

        $case_details=DB::table("cases")
                            ->where("case_test_id","=",$case_test_id)
                            ->first();
        $email_details=[];

        if($case_details->client_id==953){
            $email_det=DB::table("client_institution_f")
                            ->where("client_id","=",$case_details->client_id)
                            ->where("institution_id","=",$case_details->institution_id)
                            ->first();
            
            $email_details["inst_email"]=@$email_det->institution_email_id;
            $email_details["phlebo_email"]=@$email_det->phlebo_email_id;
            $email_details["comm_email"]=@$email_det->common_email_id;
                            
        }elseif($case_details->project_id==262 || $case_details->project_id==263){
            $email_det=@DB::table("project_emails")
                            ->where("client_id","=",$case_details->client_id)
                            ->first();

            
            $email_details["inst_email"]=@$email_det->institution_email_id;
            $email_details["phlebo_email"]=@$email_det->phlebo_email_id;
            $email_details["comm_email"]=@$email_det->common_email_id;
        }                    

        if($email_details["inst_email"]==null || $email_details["phlebo_email"]==null || $email_details["comm_email"]==null){

            $this->logMissingDetails($case_test_id,$email_details);

        }else{
            
            missingDetails::where("case_test_id","=",$case_test_id)->delete();            


        }

        return $email_details;
    }



    public function logMissingDetails($case_test_id,$email_details){


        $miss_det=@missingDetails::where("case_test_id","=",$case_test_id)->first();
        
        if($miss_det!=null){

        
        }elseif($miss_det==null){
            $miss_det=new missingDetails;
        }            

        $case_details=DB::table("cases")
                            ->where("case_test_id","=",$case_test_id)
                            ->first(); 
        $miss_det->case_no=$case_details->case_no;
        $miss_det->case_test_id=$case_details->case_test_id;

        $miss_det->client_id=$case_details->client_id;
        $miss_det->project_id=$case_details->project_id;

        $miss_det->collection_date=$case_details->sample_collection_date;
        $miss_det->accessioning_date=$case_details->sample_received_date;

        $miss_det->phlebo_email_id=$email_details["phlebo_email"];
        $miss_det->institution_email_id=$email_details["inst_email"];
        $miss_det->common_email_id=$email_details["comm_email"];
        $miss_det->save();



    }





    public function sendEmailReport(){

        ini_set('max_execution_time', 3600);
        
        Log::info("API TO SEND FIND CASES REPORTS HAS BEEN HIT");

        $email_queues=EmailQueue::where('email_sent','=',0)
                            ->limit(15)
                            ->get();

        foreach($email_queues as $email_queue){

            $check1 = \Storage::disk('s3')->exists($email_queue->report_document);
             

            $case_report_result_type=@FindCase::where('id','=',$email_queue->case_report_id)->first()->result_type;    

            if(empty($case_report_result_type)==true || is_null($case_report_result_type)==true){
                $check2=null;
            }else{
                $check2=1;
            }


            if(@$check1 && @$check2){

                $file = \Storage::disk('s3')->get($email_queue->report_document); 
                $file_name=$email_queue->report_document;
                $case_det=FindCase::where('id','=',$email_queue->case_report_id)->first();
                $subject="Report for ".$case_det->first_name." ".$case_det->last_name." - ".
                                            $case_det->test_name." - UID:".$case_det->uid_serial_number;
                $recipient=$email_queue->recipient;                            
                $message_body="";

                Mail::raw($message_body,function($msg) use ($file,$subject,$file_name,$recipient){

                    $msg->subject($subject);
                    $msg->to($recipient);
                    $msg->attachData($file,"$file_name");

                });

                if(count(Mail::failures())==0){
                    Log::info(" MAIL_SENT FOR THE EMAIL_QUEUE_ID ".$email_queue->id);    
                    
                    $emailed_at=date("Y-m-d H:i:s");
                    
                    DB::table("email_queue")
                        ->where("id","=",$email_queue->id)
                        ->update(["email_sent"=>1,"emailed_at"=>$emailed_at]);
                    
                    DB::table("cases")
                        ->where("id","=",$email_queue->case_report_id)
                        ->update(["email_sent"=>1,"emailed_at"=>$emailed_at]);

                }
                    

            }    

        }                    

        dd("done");
    }


    public function notifyIncompCases(){

        $subject="FIND , ART and OST reports not sent ";
        $recipient="gaurav.singh@corediagnostics.in";

        $err_cases=missingDetails::get();

        Mail::send('Email_Views.missingDetailsInfo', 
                    array(  'err_cases'=>$err_cases
                    
                    function($message) use ($subject,$recipient){
                    
                        $message->subject($subject);
                        $message->to($recipient);
                        

            });

    }
    
}
