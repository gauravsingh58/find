<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Models\FindCase;
use DB;

class ExcelController extends Controller
{
    
    public function downloadExcel(){
        ini_set("memory_limit","256M");
        
        ini_set('max_execution_time',3600);
        
        $tnp="Test Not Performed";
        $nd="Non-Diagnostic";

        
        $data=FindCase::whereNotIn('result_type',[$nd,$tnp])
                    ->get()->toArray();
        

        foreach($data as &$d){
            

            
            if($d['email_sent']==1){
                $d['email_sent']="Sent";
            }elseif($d['email_sent']==2){
                $d['email_sent']="Queued";
            }else{
                $d['email_sent']="Pending";
            }


            // if($d['svr']=="SVR-HB105"){
            //     $d['svr']="SVR";
            // }else{
            //     $d['svr']="N/A";
            // }

            $expression=$d['result'];
            $result=preg_match_all("/([^0-9a-zA-Z]*)(.*)([*])(\d+)(<sup>)(\d+)(<\/sup>)/",
                $expression,$match);
            
            if($result==1){
                $initial_expression=$match[1][0];
                $first_digit=$match[2][0];
                $second_digit=pow($match[4][0],$match[6][0]);
                $product=$first_digit*$second_digit;
                $final_result=$product;
                               
            }else{
                $final_result=$d['result'];
            }

            if($d['result_type']=="Test-Cancelled" || $d['result_type']=="Test Cancelled" || 
                $d['result_type']=="Test Not Performed" || $d['result_type']=="Non-Diagnostic"){
                
                $d['tnp_nd']=@$d['result_type'];//." - ".@$final_result;
                $d['report_reason']=@$final_result;
                $d['quantitative_result']=null;
                $d['result_type']=null;
            }elseif($d['result_type']=="Not-Detected"){
                $d['tnp_nd']=null;
                $d['report_reason']=@$final_result;
                $d['quantitative_result']=null;
                $d['result_type']=$d['result_type'];//Detected
            }elseif($d['result_type']=="Detected"){
                $d['tnp_nd']=null;
                $d['report_reason']=null;//@$final_result;
                $d['quantitative_result']=@$final_result;
                $d['result_type']=$d['result_type'];//Detected
            }else{
                $d['tnp_nd']=null;
                $d['report_reason']=null;
                $d['quantitative_result']=@$final_result;
                $d['result_type']=@$d['result_type'];
                
            }

            $result2=preg_match("/(Genotype)(.*)(\d)(.*)/",
                                        $d['quantitative_result'],$match2);

            if($result2==1){
                $d['genotype_quant_result']=$match2[3];
                $d['hcv_quant_result']=null;
            }else{
                $d['genotype_quant_result']=null;
                $d['hcv_quant_result']=$d['quantitative_result'];
            }




            $d['sample_collection_date']=(@$d['sample_collection_date'])
                                                ?   date("d-m-Y", strtotime($d['sample_collection_date']))
                                                :   null;
            $d['sample_received_date']=(@$d['sample_received_date'])
                                                ?   date("d-m-Y", strtotime($d['sample_received_date']))
                                                :   null;
            $d['report_date']=(@$d['report_date'])
                                    ?   date("d-m-Y", strtotime($d['report_date']))
                                    :   null;

            $d['emailed_at']=(@$d['emailed_at'])
                                    ?   date("d-m-Y H:i:s",strtotime($d['emailed_at']))
                                    :   null;

            if($d['age']==null){
                $d['age_day']=(@$d['age_day'])
                                    ?   $d['age_day']
                                    :   0;
                
                $d['age_year']=(@$d['age_year'])
                                    ?   $d['age_year']
                                    :   0;
                $d['age_month']=(@$d['age_month'])
                                    ?   $d['age_month']
                                    :   0;                    
                                                        
                $d['age']=@$d['age_year']."Y ".@$d['age_month']."M ".@$d['age_day']."D ";
            }else{
                $d['age']=$d['age']."Y 0M 0D";
            }                        







            $d['uid']=$d['uid_serial_number'];//substr(@$d['uid_serial_number'],-4);

            $d['treating_client']=DB::table("clients_f")
                                        ->where('client_id','=',$d['client_id'])
                                        ->first()->client_name;

            $d['treating_institution']=DB::table("institutions_f")
                                        ->where('institution_id','=',$d['institution_id'])
                                        ->first()->institution_name;
            /*
            $uid_case_nos=config('custom.UID_Case_Nos');
                  
            if(in_array((int)$d['case_no'],$uid_case_nos )==true){
                $d['uid']=str_replace("/",0,$d['uid']);
            }   
            */


        }

        $final_data=[];    
        foreach($data as &$d){
            $new_row=[];
            $new_row['UID Serial no.']=$d['uid'];//substr($d['uid_serial_number'],-4);
            $new_row['Case No']=$d['case_no'];

            $new_row['Test Code']=$d['test_code'];
            $new_row['Test Name']=$d['test_name'];
            $new_row['Specimen Details']=$d['specimen_detail'];
            $new_row['Full Name']=$d['first_name']." ".$d['middle_name']." ".$d['last_name'];
            $new_row['Sex']=$d['sex'];
            $new_row['Age']=$d['age'];
            $new_row['Mobile No']=$d['mob_no'];
            $new_row['Lab num']=$d['labnum'];
            $new_row['Sample Collection Date']=$d['sample_collection_date'];
            $new_row['Sample Received Date']=$d['sample_received_date'];
            $new_row['Report Date']=$d['report_date'];
            $new_row['Treating Client']=$d['treating_client'];
            $new_row['Treating Institution']=$d['treating_institution'];

            $new_row['Genotype Quantitative Result']=$d['genotype_quant_result'];
            $new_row['HCV RNA Quantitative Result']=$d['hcv_quant_result'];

            
            $new_row['Result']=$d['result_type'];//$d['result_type'];
            
            
            // if(Auth::user()->is_admin == 1 || Auth::user()->id==217){
            //     $new_row['TNP/ND']=$d['tnp_nd'];
            //     $new_row['Reason']=$d['report_reason'];
            //     $new_row['Email Sent At']=$d['emailed_at'];
            // }
            
            //$new_row['SVR']=$d['svr'];
            
            //$new_row['Email Status']=$d['email_sent'];
            
            array_push($final_data,$new_row);
    }    
        date_default_timezone_set("Asia/Kolkata");
        
        $filename="Find_Cases_".date("d_m_Y_H_i_s");
        
        return \Excel::create('Details',function($excel) use ($final_data){
            $excel->sheet('Medical Details',function($sheet) use ($final_data)
            {
                //$sheet->getDefaultStyle()->getFont()->setSuperScript(true);
               // $sheet->getDefaultStyle()->getFont()->setSuperScript(true);                
                $sheet->freezeFirstRow();
                $sheet->fromArray($final_data,null,'A1',true);

            });
        })
        ->setFileName($filename)
        ->download();

    }
    

    /*
    public function downloadExcel(){

        $cases_data=FindCase::get();


        $final_data=[];

        foreach($cases_data as $case_data){

            $final_data_row=[];
            $final_data_row['UID Serial no.']=$row->accessioning_date;
            $final_data_row['Case No']=$row->accessioning_date;
            $final_data_row['Test Code']=$row->accessioning_date;
            $final_data_row['Test Name']=$row->accessioning_date;
            $final_data_row['Specimen Details']=$row->accessioning_date;
            $final_data_row['Full Name']=$row->accessioning_date;
            $final_data_row['Sex']=$row->accessioning_date;
            $final_data_row['Age']=$row->accessioning_date;
            $final_data_row['Mobile No']=$row->accessioning_date;
            $final_data_row['Lab num']=$row->accessioning_date;
            $final_data_row['Sample Collection Date']=$row->accessioning_date;
            $final_data_row['Sample Received Date']=$row->accessioning_date;

            $final_data_row['Report Date']=$row->accessioning_date;
            $final_data_row['Treating Hospital']=$row->accessioning_date;
            $final_data_row['Genotype Quantitative Result']=$row->accessioning_date;
            $final_data_row['HCV RNA Quantitative Result']=$row->accessioning_date;

            $final_data_row['Result']=$row->accessioning_date;
            $final_data_row['TNP/ND']=$row->accessioning_date;

            $final_data_row['Reason']=$row->accessioning_date;



        }

    }
    */


}
