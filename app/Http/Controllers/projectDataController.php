<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use App\Models\FindCase;

use App\Models\dataFetchLog;

use Log;

class projectDataController extends Controller
{
    //
    public function syncProjectCases(){
        // $entry=new dataFetchLog();
        // $entry->last_requested_at=date("Y-m-d H:i:s");
        // $entry->save();
        // dd(date("Y-m-d H:i:s",strtotime("-15 minutes")));
        //$date_time="2020-07-15 00:00:00";
        $date_time=dataFetchLog::orderBy('id', 'DESC')->first()->last_requested_at;

        //$previous_requests=dataFetchLog::delete();

        


        Log::info(" api to fetch project data from crick is hit ");

        

        $data_s=@Curl::to('https://lims.crick.corediagnostics.in/api/getProjectData/'.$date_time)
                            ->get(); 
        
        if($data_s!=null){
            
            $entry=new dataFetchLog();
            $entry->last_requested_at=date("Y-m-d H:i:s",strtotime("-15 minutes"));
            $entry->save();
                
        }
                            
        $data=json_decode($data_s);  
        //dd($data);

        foreach($data as $d){

            $data_check=FindCase::where('test_trf_id','=',$d->test_trf_id)->first();

            if($data_check==null){

                $case_entry=new FindCase;
                Log::info(" details for case_no ".$d->case_no." with case_test_id ".$d->test_trf_id." under project_id ".$d->project_id." CREATED ");
            }else{
                
                $case_entry=$data_check;
                Log::info(" details for case_no ".$d->case_no." with case_test_id ".$d->test_trf_id." under project_id ".$d->project_id." UPDATED ");
            }   
                $case_entry->case_id=$d->case_no;
                $case_entry->case_test_id=$d->test_trf_id;
                $case_entry->project_id=$d->project_id;
                
                $case_entry->client_id=$d->client_id;
                $case_entry->institution_id=$d->institution_id;
                $case_entry->uid_serial_number=$d->uid_serial_number;
                $case_entry->id1=$d->id1;
                $case_entry->pid=$d->pid;
                
                $case_entry->case_no=$d->case_no;

                $case_entry->test_trf_id=$d->test_trf_id;
                $case_entry->test_code=$d->test_code;
                $case_entry->test_name=$d->test_name;
                $case_entry->specimen_detail=$d->specimen_detail;
                $case_entry->first_name=$d->first_name;
                $case_entry->middle_name=$d->middle_name;
                $case_entry->last_name=$d->last_name;
                $case_entry->sex=$d->sex;
                $case_entry->dob=$d->dob;
                $case_entry->age=$d->age;
                $case_entry->mob_no=$d->mob_no;
                $case_entry->labnum=$d->labnum;

                if($d->sample_collection_date=="0000-00-00 00:00:00"){
                    continue;
                }                

                $case_entry->sample_collection_date=$d->sample_collection_date;
                $case_entry->sample_received_date=$d->sample_received_date;
                $case_entry->report_date=$d->report_date;
                $case_entry->report_document=$d->report_document;
                $case_entry->treating_hospital=$d->treating_hospital;
                $case_entry->units=$d->units;
                $case_entry->result=$d->result;
                $case_entry->result_type=$d->result_type;
                //$case_entry->email_sent=0;
                $case_entry->emailed_at=null;
                $case_entry->accessioning_date=null;
                $case_entry->reporting_date=$d->report_date;
                $case_entry->workflow_state_id=$d->workflow_state_id;
                $case_entry->current_status=$d->current_status;
                $case_entry->age_year=$d->ageYear;
                $case_entry->age_month=$d->ageMonth;
                $case_entry->age_day=$d->ageDay;
                $case_entry->save();


        }

        // dd("doen");
    }

}
