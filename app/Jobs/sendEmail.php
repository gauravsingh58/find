<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Models\FindCase;

class sendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $case_entry_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->case_entry_id=$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        
        //$entry_id=
        $this->sendEmailReport();


    }

    public function sendEmailReport(){
        
        ini_set('max_execution_time', 3600);

        $entries=FindCase::where('email_sent','=',0)->get();
        
        foreach($entries as $row){

            $check = \Storage::disk('s3')->exists($row->report_document);
             


            $case_report_result_type=@DB::table('case_reports')
                                        ->where('id','=',$row->case_report_id)
                                        ->first()->result_type;

            if(empty($case_report_result_type)==true || is_null($case_report_result_type)==true){
                $check2=null;
            }else{
                $check2=1;
            }
            $to = $row->to_address;
            
            if(@$check && @$check2){
                $case_report = Report::find($row->case_report_id);
                $file = \Storage::disk('s3')->get($row->pdf_file); 
                
                if(@$to){

                    \Mail::send([], [], function($message) use ($file,$row,$to) {
                        $message->replyTo('coresponse@corediagnostics.in');
                        $message->to($to)->subject($row->email_subject);
                        //$message->bcc("patient.reports@corediagnostics.in");
                        $message->setBody($row->email_body,'text/html');
                        $message->attachData($file, $row->pdf_file);
                    });

                    $row->status = 2;
                    $case_report->email_sent = 1;
                    $case_report->save();
                    date_default_timezone_set("Asia/Kolkata");
                    $current_timestamp=date("Y-m-d H:i:s");
                    
                    DB::table('case_reports')
                         ->where('id','=',$case_report->id)
                         ->update(['emailed_at'=>$current_timestamp]);
      
                    $case_no=DB::table('case_reports')
                                            ->where('id',$row->case_report_id)
                                            ->first()->case_no;    
  
                    Log::info(" EMAIL_SENT AT ".$current_timestamp." TO ".$to." FOR CASE_NO ".$case_no.
                                                        " WITH EMAIL_QUEUE ID ".$row->id);
                }    
            }else{
                $current_timestamp=date("Y-m-d H:i:s");
                $case_no=DB::table('case_reports')
                                ->where('id',$row->case_report_id)
                                ->first()->case_no;    

                Log::info(" EMAIL_NOT_SENT AT ".$current_timestamp." TO ".$to." FOR CASE_NO ".$case_no." WITH EMAIL_QUEUE ID ".$row->id);
                $row->status = 3;
            }


            $row->save();
        }
        $status = true;
        
        return Response::json(array('success'=>$status));
    }



}
